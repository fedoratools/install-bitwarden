#!/bin/bash

# Verifică dacă sudo este instalat
if ! command -v sudo &> /dev/null; then
  echo "Sudo nu este instalat. Instalarea nu poate continua."
  exit 1
fi

# Verifică dacă curl este instalat, dacă nu, îl instalează
if ! command -v curl &> /dev/null; then
  sudo dnf install -y curl
fi

# Actualizează lista de pachete și sistemul
sudo dnf update -y

# Instalează wget dacă nu este deja instalat
if ! command -v wget &> /dev/null; then
  sudo dnf install -y wget
fi

# URL pentru Bitwarden
BITWARDEN_URL="https://vault.bitwarden.com/download/?app=desktop&platform=linux&variant=rpm"

# Descarcă fișierul RPM pentru Bitwarden folosind curl pentru a urma redirectările
curl -L "$BITWARDEN_URL" -o bitwarden.rpm

# Instalează fișierul RPM
sudo dnf install -y ./bitwarden.rpm

# Curăță fișierul RPM descărcat
rm -f bitwarden.rpm

echo "Instalarea Bitwarden s-a finalizat cu succes!"

