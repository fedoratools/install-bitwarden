# Script de Instalare Bitwarden pentru Fedora

Acest script automatizează procesul de instalare a aplicației Bitwarden pe o distribuție Linux care folosește `dnf` (cum ar fi Fedora).

## Descriere

Scriptul efectuează următoarele acțiuni:
1. Verifică dacă `sudo` este instalat pe sistem.
2. Verifică dacă `curl` este instalat și, dacă nu, îl instalează.
3. Actualizează lista de pachete și sistemul.
4. Verifică dacă `wget` este instalat și, dacă nu, îl instalează.
5. Descarcă fișierul RPM pentru Bitwarden folosind `curl`.
6. Instalează fișierul RPM descărcat.
7. Șterge fișierul RPM descărcat pentru curățenie.
8. Afișează un mesaj de succes la final.

## Utilizare

Pentru a utiliza acest script, urmați pașii de mai jos:

1. Descărcați scriptul și faceți-l executabil:
   ```bash
   chmod +x install_bitwarden.sh
   ```

2. Rulați scriptul:
   ```bash
   ./install_bitwarden.sh
   ```

## Pre-rechizite

- Distribuție Linux care folosește `dnf` (Fedora, RHEL, CentOS etc.).
- Conexiune la internet activă pentru a descărca pachetele necesare.

## Note

- Asigurați-vă că rulați scriptul cu privilegii de administrator pentru a permite instalarea pachetelor.
- Scriptul este creat pentru a fi utilizat pe sisteme care folosesc `dnf`. Pentru alte gestioare de pachete, adaptările sunt necesare.